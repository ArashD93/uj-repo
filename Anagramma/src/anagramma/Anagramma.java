package anagramma;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;

public class Anagramma {

    public static void main(String[] args) throws FileNotFoundException {
        
        //1. Kérjen be a felhasználótól egy szöveget, majd határozza meg, hogy hány különböző
        //karakter található a szövegben! A darabszámot és a karaktereket írja ki a képernyőre!
        
        Scanner sc;
        /*
        sc = new Scanner(System.in);
        System.out.println("Karakterek kigyűjtése beolvasott szövegből.\n"
                + "Kérem adja meg a szöveget: ");
        String askedString=sc.nextLine();
        charCounter(askedString);
        System.out.println();
        */
        
        //2. Olvassa be a szotar.txt állományból a szavakat, és a következő feladatok megoldása
        //során ezekkel dolgozzon! Amennyiben nem tudja beolvasni az állományból a szavakat,
        //akkor az első 10 szóval dolgozzon!
        
        ArrayList<String> vocabulary = new ArrayList<>();
        
        try{
            sc = new Scanner(new File("src/anagramma/szotar.txt"));
            while(sc.hasNext()){
                vocabulary.add(sc.nextLine());
            }
        }catch (FileNotFoundException fnfe){
            System.out.println("A file nem létezik.");
            System.exit(0);
        }
        
        //3. Az állományból beolvasott szavakat alakítsa át úgy, hogy minden szó karaktereit
        //egyenként tegye ábécérendbe! Az így létrehozott szavakat írja ki az abc.txt állományba
        //az eredeti állománnyal egyező sorrendben!
        
        /*
        for (String s : vocabulary) {
            System.out.println(order(s));
        }
        System.out.println();
        */
        
        //4. Kérjen be a felhasználótól két szót, és döntse el, hogy a két szó anagramma-e! Ha azok
        //voltak, írja ki a képernyőre az „Anagramma” szót, ha nem, akkor pedig a „Nem anagramma” szöveget!
        
        /*
        sc = new Scanner(System.in);
        System.out.print("Két szórol eldöntjük, hogy anagramma-e.\nElső szó: ");
        String word1=sc.next();
        System.out.print("Második szó: ");
        String word2=sc.next();
        System.out.println( isAnagramma(word1,word2) ? "Anagramma\n" : "Nem anagramma\n");
        */
        
        //5. Kérjen be a felhasználótól egy szót! A szotar.txt állomány szavaiból keresse meg
        //a szó anagrammáit (a szót önmagát is annak tekintve)! Ha van találat, azokat egymás alá
        //írja ki a képernyőre, ha nem volt találat, akkor írja ki a „Nincs a szótárban
        //anagramma” szöveget!
        
        /*
        sc = new Scanner(System.in);
        System.out.print("Egy szó anagrammáit keressük a szótárban.\nA vizsgált szó: ");
        String word3=sc.next();
        word3=order(word3);
        boolean haveFound=false;
        for (String s : vocabulary) {
            if (order(s).equals(word3)){
                System.out.println(s);
                haveFound=true;
            }
        }
        if (!haveFound) System.out.println("Nincs a szótárban anagramma!");
        System.out.println();
        */
        
        //6. Határozza meg, hogy a szotar.txt állományban melyik a leghosszabb szó! Ha több,
        //ugyanannyi karakterből álló leghosszabb szó volt, akkor az ugyanazokat a karaktereket
        //tartalmazó szavakat (amelyek egymás anagrammái) közvetlenül egymás alá írja ki!
        //A feltételnek megfelelő összes szó pontosan egyszer szerepeljen a kiírásban!
        
        //longestWord(vocabulary);
        
        //7. Rendezze a szotar.txt állományban lévő szavakat a karakterek száma szerint növekvő
        //sorrendbe! Az egyforma hosszúságú és ugyanazokat a karaktereket tartalmazó szavak
        //(amelyek egymás anagrammái) szóközzel elválasztva ugyanabba a sorba kerüljenek!
        //Az egyforma hosszúságú, de nem ugyanazokat a karaktereket tartalmazó szavak külön
        //sorba kerüljenek! A különböző hosszúságú szavakat egy üres sorral különítse el egymástól!
        //Az így rendezett szavakat írja ki a rendezve.txt állományba!
        
        /*
        orderByLength(vocabulary);
        orderByAnagramma(vocabulary);
        print(vocabulary);
        */
        
        //+1 anagramma feladat:
        //Írjunk egy metódust, ami legenerálja egy String összes lehetséges anagrammáját.
        //Bónuszként megpróbálhatjuk kiválogatni belőlük az értelmes anagrammákat
        //(ehhez használjunk, valamilyen a netről letölthető szótár állományt, ami tartalmazza a magyar szavakat).

        HashSet<String> anagramma = new HashSet<>();
        String askedWord = "teszt";
        generateAnagramma(askedWord,anagramma);
        for (String s : anagramma) {
            System.out.println(s);
        }
        
    }

    private static void charCounter(String s) {
        HashSet<Character> charSet = new HashSet<>();
        for(int i=0;i<s.length();i++) charSet.add(s.charAt(i));
        for (Character c : charSet) {
            int charCount=0;
            for (int i=0;i<s.length();i++){
                if (c==s.charAt(i)){
                    charCount++;
                }
            }
            System.out.println( c==' ' ? "space: " + charCount + " db" : c + ": " + charCount + " db");
        }
    }
    
    /**
     * Egy szó betűit abc sorrendbe rakja. Pl. arab-->aabr
     * @param s:String
     * @return 
     */
    private static String order(String s) {
        String output="";
        ArrayList<Character> word = new ArrayList<>();
        for (int i=0;i<s.length();i++){
            word.add(s.charAt(i));
        }
        while (word.size()>0){
            char minChar=word.get(0);
            for (int j=1;j<word.size();j++){
                if (word.get(j)<minChar) minChar=word.get(j);
            }
            output=output+minChar;
            //itt castolni kell, mert a remove metódus Character-t vár
            word.remove((Character)minChar);
        }
        return output;
    }

    private static boolean isAnagramma(String a, String b) {
        return (order(a).equals(order(b)));
    }

    private static void longestWord(ArrayList<String> vocabulary) {
        int maxLength=vocabulary.get(0).length();
        for (String s : vocabulary) {
            if (s.length()>maxLength) maxLength=s.length();
        }
        ArrayList<String> longestWords = new ArrayList<>();
        for (String s : vocabulary) {
            if (s.length()==maxLength) longestWords.add(s);
        }
        System.out.println("A leghosszabb szavak (anagrammánként rendezve): ");
        while (longestWords.size()>0){
            String firstWord=order(longestWords.get(0));
            Iterator<String> longestWordsIterator = longestWords.iterator();
            while (longestWordsIterator.hasNext()){
                String word=longestWordsIterator.next();
                if (order(word).equals(firstWord)){
                    System.out.println(word);
                    longestWordsIterator.remove();
                }
            }
        }
        System.out.println();
    }

    private static void orderByLength(ArrayList<String> vocabulary) {
        ArrayList<String> orderedVocabulary = new ArrayList<>();
        while(vocabulary.size()>0){
            int maxLength=vocabulary.get(0).length();
            for (String s : vocabulary) {
                if (s.length()<maxLength) maxLength=s.length();
            }
            for (String s : vocabulary) {
                if (s.length()==maxLength) orderedVocabulary.add(s);
            }
            vocabulary.removeAll(orderedVocabulary);
        }
        vocabulary.addAll(orderedVocabulary);
    }

    private static void orderByAnagramma(ArrayList<String> vocabulary) {
        ArrayList<String> orderedVocabulary = new ArrayList<>();
        while(vocabulary.size()>0){
            String temp = order(vocabulary.get(0));
            for (String s : vocabulary) {
                if (order(s).equals(temp)) orderedVocabulary.add(s);
            }
            vocabulary.removeAll(orderedVocabulary);
        }
        vocabulary.addAll(orderedVocabulary);
    }

    private static void print(ArrayList<String> vocabulary) {
        System.out.print(vocabulary.get(0));
        for(int i=1;i<vocabulary.size();i++){
            if (order(vocabulary.get(i)).equals(order(vocabulary.get(i-1)))){
                System.out.print(" " + vocabulary.get(i));
            }else if(vocabulary.get(i).length()==vocabulary.get(i-1).length()){
                System.out.print("\n" + vocabulary.get(i));
            }else System.out.print("\n\n" + vocabulary.get(i));
        }
        System.out.println("\n");
    }

    private static void generateAnagramma(String askedWord, HashSet<String> anagramma) {
        ArrayList<Character> charsOfWord = new ArrayList<>();
        for(int i=0;i<askedWord.length();i++){
            charsOfWord.add(askedWord.charAt(i));
        }
        String tempS="";
        anagramma(charsOfWord,anagramma,tempS,askedWord.length());
    }

    private static void anagramma(ArrayList<Character> charsOfWord, HashSet<String> anagramma, String tempS,int l) {
        Iterator<Character> charsIterator = charsOfWord.iterator();
        while (charsIterator.hasNext()){
            Character c = charsIterator.next();
            tempS=tempS + c;
            charsIterator.remove();
            if(tempS.length()==l)anagramma.add(tempS);
            else anagramma(charsOfWord,anagramma,tempS,l);
        }
    }
   
    
    
    
    
}
